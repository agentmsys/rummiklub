import Test.HUnit
import System.Exit
import qualified Data.Set as S
import Rummiklub

main :: IO ()
main = do
  tcounts <- runTestTT (TestList tests)
  if errors tcounts + failures tcounts > 0 then exitFailure else exitSuccess
  where
    tests = [testPublicValidation]
    
testPublicValidation :: Test 
testPublicValidation = TestCase $ do
  let validGroup1 = [Standard 4 Blue,
                     Standard 4 Orange,
                     Standard 4 Red]
      validSequence1 = [Standard 1 Blue, 
                        Standard 2 Blue,
                        Standard 3 Blue]
  assertBool "simple colors" (isSameColor [Standard 3 Blue, Joker 1 (Pinned 3 Blue)])
  assertBool "simple group" (isValidGroup validGroup1)
  assertBool "simple sequence" (isValidSequence validSequence1)

  assertEqual "simple public" (Right ()) (isValidPublic (Public [validGroup1, validSequence1]))
                                                         
  