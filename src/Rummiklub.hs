{-% LANGUAGE ScopedTypeVariables %-}
module Rummiklub where
import qualified Data.Set as S
import Data.Maybe
import Data.List
import qualified Data.Map as M
import Control.Monad
import Control.Monad.Random
import System.Random.Shuffle

import Debug.Trace

data Color = Blue | Orange | Red | Black
           deriving (Show, Eq, Ord)

data Pinned = Pinned Int Color | Unpinned
            deriving (Show, Eq, Ord)

data Tile = Standard Int Color | Joker Int Pinned
            deriving (Show, Eq, Ord)
                     
type Pool = [Tile]

allTiles :: Pool
allTiles = [Joker 1 Unpinned, Joker 2 Unpinned] ++ numericTiles
  where
    numericTiles = [Standard i c | i <- [1 .. 13],
                                   c <- [Blue, Orange, Red, Black],
                                   _ <- [1 :: Int,2]]
                   
randomPool :: MonadRandom m => m Pool   
randomPool = shuffleM allTiles
                   
type Run = [Tile]

{-
isValidRun :: Run -> Bool
isValidRun run = length run >= 3 && (isValidSequence run || isValidGroup run)
-}

isValidSequence :: Run -> Bool
isValidSequence run = length run >= 3 && isSameColor run && isSequence
  where
    isSequence = isJust numFolder
    numFolder = foldl (\accum tile -> let prevCmp num1 nextTile = case nextTile of 
                                            Joker _ Unpinned -> 
                                              case S.toList (colors run) of
                                                [] -> error "no colors- impossible"
                                                commonColor:[] -> Just (Standard (num1 + 1) commonColor)
                                                _ -> error "no common color- impossible"
                                            Joker _ (Pinned nextNum _) -> valCmp num1 nextNum nextTile
                                            Standard nextNum _ -> valCmp num1 nextNum nextTile
                                          valCmp num1 num2 nextTile = if num1 + 1 == num2 then
                                                               Just nextTile else Nothing
                                      in
                                       case accum of
                                         Nothing -> Nothing
                                         j@(Just (Joker _ Unpinned)) -> j
                                         Just (Joker _ (Pinned prevNum _)) -> prevCmp prevNum tile
                                         Just (Standard prevNum _) -> prevCmp prevNum tile


                      ) (Just (head run)) (tail run)
                
colors :: Run -> S.Set Color
colors run = foldr (\tile accum -> case tile of
                       Standard _ color -> S.insert color accum
                       Joker _ Unpinned -> accum
                       Joker _ (Pinned _ color) -> S.insert color accum) S.empty run
  
isSameColor :: Run -> Bool
isSameColor run = S.size (colors run) == 1
             
isValidGroup :: Run -> Bool             
isValidGroup run  = (length run == 3 || length run == 4) && isJust diffColors
  where 
    folder _ Nothing = Nothing
    folder (Standard _ color) (Just seenColors) = if S.member color seenColors then Nothing else Just (S.insert color seenColors)
    folder (Joker _ Unpinned) colors'@(Just _) = colors'
    folder (Joker _ (Pinned _ color)) (Just seenColors) = if S.member color seenColors then Nothing else Just (S.insert color seenColors)
    diffColors = foldr folder (Just S.empty) run

data Public = Public [Run]

data ValidationError = InvalidSequence Run |
                       InvalidGroup Run |
                       InvalidTileCount Tile |
                       JokerIsNotPinned Tile
                       deriving (Show, Eq)

isValidPublic :: Public -> Either ValidationError ()
isValidPublic (Public runs) = do
  --check that colors are valid for each run
  mapM_ mapper runs
  --count that we have no more than two of each tile
  foldM_ folder M.empty (concat runs)
  --ensure that all jokers in the public space are pinned
  mapM_ (mapM (\tile -> case tile of 
                  Joker _ Unpinned -> Left (JokerIsNotPinned tile)
                  _ -> pure ()
              )) runs  
  where
    mapper run = if not (isSameColor run) then 
                   if not (isValidGroup run) then
                     Left (InvalidGroup run)
                   else
                     pure ()
                 else 
                   if not (isValidSequence run) then
                     Left (InvalidSequence run)
                   else
                     pure ()
    folder accum tile = case M.lookup tile accum of
      Just count -> if count == 2 then
                      Left (InvalidTileCount tile)
                    else
                      Right (M.insert tile (count + 1 :: Int) accum)
      Nothing -> Right (M.insert tile 1 accum) 
                     
data Hand = Hand [Tile]

type Position = Int
                     
data Move = MovePublic Tile Run Run Position | -- shifts a tile from the first run to the second run
            DropTile Tile Run Position | -- drops a tile from the player's hand to the position of the run
            PickUpTile Tile Run | -- take a tile from the public space and put it in your hand
            DropRun Run | -- drop a sequence or group to the public space
            SplitRun Run Position |  --split a public run in preparation for adding a new tile
            TakeFromPool -- take a random tile from the pool 
            
data MoveError = NoSuchPublicRun Run |
                 NoSuchTargetTileInRun Tile Run |
                 NoSuchTileInHand Tile |
                 RunIndexOutOfBounds Position Run |
                 NoSuchRunInHand Run |
                 MissingTiles [Tile] |
                 TilesCreated [Tile] | 
                 JokerPreservationError |
                 EmptyPool |
                 InvalidPublic ValidationError
                 deriving Show
                 
insertIntoRun :: Position -> Tile -> Run -> Either MoveError Run
insertIntoRun pos tile run = if pos < 0 || pos > length run then
                               Left (RunIndexOutOfBounds pos run)
                             else
                               Right (as ++ (tile:bs))
  where (as,bs) = splitAt pos run
        
splitRun :: Position -> Run -> Either MoveError (Run, Run)        
splitRun pos run = if pos < 0 || pos > length run then
                     Left (RunIndexOutOfBounds pos run)
                   else
                     Right (splitAt pos run)
                 
deleteTileInPublicRun :: Tile -> Run -> Public -> Either MoveError Public                 
deleteTileInPublicRun target run (Public runs) = if elem run runs then
                                                   Left (NoSuchPublicRun run)
                                                 else if not (elem target run) then
                                                        Left (NoSuchTargetTileInRun target run)
                                                      else
                                                        let newRun = delete target run
                                                            newPub = delete run runs
                                                            newPub' = newRun : newPub in
                                                        Right (Public newPub')
                                                        
addTileInPublicRun :: Tile -> Position -> Run -> Public -> Either MoveError Public
addTileInPublicRun newTile pos run (Public runs) = if notElem run runs then
                                                 Left (NoSuchPublicRun run)
                                               else do
                                                 newRun <- insertIntoRun pos newTile run
                                                 let newPub = delete run runs
                                                     newPub' = newRun : newPub
                                                 Right (Public newPub')

deleteTileInHand :: Tile -> Hand -> Either MoveError Hand
deleteTileInHand tile (Hand hand) = if notElem tile hand then
                                         Left (NoSuchTileInHand tile)
                                       else
                                         Right (Hand (delete tile hand))
                                                                   
addTileToHand :: Tile -> Hand -> Either MoveError Hand
addTileToHand tile (Hand handSet) = Right (Hand (tile : handSet))

addRunToPublic :: Run -> Public -> Either MoveError Public
addRunToPublic run (Public pubs) = Right (Public (run : pubs))

deleteRunInPublic :: Run -> Public -> Either MoveError Public
deleteRunInPublic run (Public pubs) = if notElem run pubs then
                                        Left (NoSuchPublicRun run)
                                      else
                                        Right (Public (delete run pubs))
                                                 
executeMove :: Move -> Hand -> Public -> Pool -> Either MoveError (Public, Hand, Pool)
executeMove (MovePublic target origRun destRun pos) hand pub pool = do
  pub' <- deleteTileInPublicRun target origRun pub
  pub'' <- addTileInPublicRun target pos destRun pub'
  pure (pub'', hand, pool)
executeMove (DropTile targetTile run pos) hand pub pool = do
  hand' <- deleteTileInHand targetTile hand
  pub' <- addTileInPublicRun targetTile pos run pub
  pure (pub', hand', pool)
executeMove (PickUpTile tile run) hand pub pool = do
  pub' <- deleteTileInPublicRun tile run pub
  hand' <- addTileToHand tile hand
  pure (pub', hand', pool)
executeMove (DropRun run) hand pub pool = do   
  hand' <- foldM (\accum tile -> deleteTileInHand tile accum) hand run
  pub' <- addRunToPublic run pub
  pure (pub', hand', pool)
executeMove (SplitRun run pos) hand pub pool = do
  pub' <- deleteRunInPublic run pub
  (run1, run2) <- splitRun pos run
  pub'' <- addRunToPublic run1 pub'
  pub''' <- addRunToPublic run2 pub''
  pure (pub''', hand, pool)
executeMove TakeFromPool hand pub pool = if length pool == 0 then
                                      Left EmptyPool
                                    else do
                                      hand' <- addTileToHand (head pool) hand
                                      Right (pub, hand', (tail pool))
                                      
  
  
data SingleStepMove = DropTilesMove (Hand, Public) |
                      TakeTileFromPoolMove
                      
data Engine = MultiStepEngine String (Hand -> Public -> [Move]) | --returns a series of moves
              SingleStepEngine String (Hand -> Public -> SingleStepMove) -- returns a new hand and pub without showing moves
                                  
engineName :: Engine -> String
engineName (MultiStepEngine name _) = name                                  
engineName (SingleStepEngine name _) = name

-- an engine which take a tile every turn
stupidEngine :: String -> Engine
stupidEngine name = MultiStepEngine name f
  where
    f _ _ = [TakeFromPool]
    
runOneTurn :: Engine -> Hand -> Public -> Pool -> Either MoveError (Public, Hand, Pool)    
runOneTurn (MultiStepEngine _ func) hand pub pool = do 
  let moves = func hand pub
  foldM (\(pub', hand', pool') move -> executeMove move hand' pub' pool') (pub, hand, pool) moves
runOneTurn (SingleStepEngine _ func) hand@(Hand handTiles) pub@(Public runs) pool = case func hand pub of
  DropTilesMove (hand'@(Hand handTiles'), pub'@(Public runs')) -> do
    --validate that the same tiles appear in the old hand + old pub vs. new hand + new pub
    --special joker handling- jokers must become pinned in the new version
    let isJoker tile = case tile of 
          Joker _ _ -> True
          _ -> False
        isPinnedJoker tile = case tile of
          Joker _ (Pinned _ _) -> True
          _ -> False
        origPubAndHand = handTiles' ++ concat runs'
        newPubAndHand = handTiles ++ concat runs
    --check if a tile has disappeared from the original hand and pub
    case (filter (not . isJoker) origPubAndHand) \\ (filter (not . isJoker) newPubAndHand) of
      missing@(_:_) -> Left (MissingTiles missing)
      [] -> pure ()
    case (filter (not . isJoker) newPubAndHand) \\ (filter (not . isJoker) origPubAndHand) of
      extra@(_:_) -> Left (TilesCreated extra)
      [] -> pure ()
    --check that all pinned in the orig pub are still in play
    case filter isPinnedJoker origPubAndHand \\ filter isPinnedJoker newPubAndHand of    
      missing@(_:_) -> Left (MissingTiles missing)
      [] -> pure ()
    --check that a joker count is the same
    --remove all pinned jokers which appeared before and after, then the remaining jokers must have been unpinned in the hand and now pinned in the public space
    when (filter isJoker newPubAndHand /= filter isJoker origPubAndHand) (Left JokerPreservationError)
    pure (pub', hand', pool)
  TakeTileFromPoolMove -> executeMove TakeFromPool hand pub pool
  
data FinishResult = EmptyPoolResult | PlayerWinsResult String
                  deriving Show

runToFinish :: [Engine] -> [Hand] -> Public -> Pool -> Either (String, MoveError) FinishResult
runToFinish engines startingHands pub pool = go (cycle engines) pub handsMap pool
  where
    handsMap :: M.Map String Hand
    handsMap = M.fromList (zip (map engineName engines) startingHands)
    go engInf pub' namedHands pool' = do
      let hand = namedHands M.! name
          currentEng = head engInf
          name = engineName currentEng
      case runOneTurn currentEng hand pub' pool' of
        Left err -> Left (name, err)
        Right (pub'', hand''@(Hand handTiles), pool'') -> do
          --check for valid public
          case isValidPublic pub'' of
            Left err -> Left (name, InvalidPublic err)
            Right _ -> if length handTiles == 0 then --check for winner
                         pure (PlayerWinsResult name)
                       else
                         go (tail engInf) pub'' (M.insert name hand'' namedHands) pool''

  