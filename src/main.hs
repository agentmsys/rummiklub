import Control.Monad.Random
import System.Random.Shuffle
import Rummiklub


main :: IO ()
main = do
  pool <- evalRandIO randomPool
  --disperse the inital card set
  players <- evalRandIO (shuffleM [stupidEngine "Stupid1", stupidEngine "Stupid2"])
  let startingHands = map Hand [take 14 (drop (c * 14) pool) | c <- [0 .. (length players - 1)]]
      startingPool = drop (14 * (length players)) pool
  case runToFinish players startingHands (Public []) startingPool of
    Left (playerName, err) -> putStrLn (playerName ++ " made a mistake: " ++ show err)
    Right result -> putStrLn (show result)
